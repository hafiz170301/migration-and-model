class NilaisController < ApplicationController
    before_action :set_nilai, only: [:show, :update, :destroy]
  
    # GET /nilais
    def index
      @nilais = Nilai.all
  
      render json: @nilais
    end
  
    # GET /nilais/1
    def show
      render json: @nilai
    end
  
    # POST /nilais
    def create
      @nilai = Nilai.new(nilai_params)
  
      if @nilai.save
        render json: @nilai, status: :created, location: @nilai
      else
        render json: @nilai.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /nilais/1
    def update
      if @nilai.update(nilai_params)
        render json: @nilai
      else
        render json: @nilai.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /nilais/1
    def destroy
      @nilai.destroy
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_nilai
        @nilai = Nilai.find(params[:id])
      end
  
      # Only allow a list of trusted parameters through.
      def nilai_params
        params.require(:nilai).permit(:nim, :kdmk, :nilai)
      end
  end