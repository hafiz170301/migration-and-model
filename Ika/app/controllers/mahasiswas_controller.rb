class MahasiswasController < ApplicationController
    before_action :set_mahasiswa, only: [:show, :update, :destroy]
  
    # GET /mahasiswas
    def index
      @mahasiswas = Mahasiswa.all
  
      render json: @mahasiswas
    end
  
    # GET /mahasiswas/1
    def show
      render json: @mahasiswa
    end
  
    # POST /mahasiswas
    def create
      @mahasiswa = Mahasiswa.new(mahasiswa_params)
  
      if @mahasiswa.save
        render json: @mahasiswa, status: :created, location: @mahasiswa
      else
        render json: @mahasiswa.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /mahasiswas/1
    def update
      if @mahasiswa.update(mahasiswa_params)
        render json: @mahasiswa
      else
        render json: @mahasiswa.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /mahasiswas/1
    def destroy
      @mahasiswa.destroy
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_mahasiswa
        @mahasiswa = Mahasiswa.find(params[:id])
      end
  
      # Only allow a list of trusted parameters through.
      def mahasiswa_params
        params.require(:mahasiswa).permit(:nim, :namaMhs, :npm)
      end
  end