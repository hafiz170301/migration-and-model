class DosensController < ApplicationController
    before_action :set_dosen, only: [:show, :update, :destroy]
  
    # GET /dosens
    def index
      @dosens = Dosen.all
  
      render json: @dosens
    end
  
    # GET /dosens/1
    def show
      render json: @dosen
    end
  
    # POST /dosens
    def create
      @dosen = Dosen.new(dosen_params)
  
      if @dosen.save
        render json: @dosen, status: :created, location: @dosen
      else
        render json: @dosen.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /dosens/1
    def update
      if @dosen.update(dosen_params)
        render json: @dosen
      else
        render json: @dosen.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /dosens/1
    def destroy
      @dosen.destroy
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_dosen
        @dosen = Dosen.find(params[:id])
      end
  
      # Only allow a list of trusted parameters through.
      def dosen_params
        params.require(:dosen).permit(:npm, :namaDsn)
      end
  end