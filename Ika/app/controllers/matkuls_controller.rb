class MatkulsController < ApplicationController
    before_action :set_matkul, only: [:show, :update, :destroy]
  
    # GET /matkuls
    def index
      @matkuls = Matkul.all
  
      render json: @matkuls
    end
  
    # GET /matkuls/1
    def show
      render json: @matkul
    end
  
    # POST /matkuls
    def create
      @matkul = Matkul.new(matkul_params)
  
      if @matkul.save
        render json: @matkul, status: :created, location: @matkul
      else
        render json: @matkul.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /matkuls/1
    def update
      if @matkul.update(matkul_params)
        render json: @matkul
      else
        render json: @matkul.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /matkuls/1
    def destroy
      @matkul.destroy
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_matkul
        @matkul = Matkul.find(params[:id])
      end
  
      # Only allow a list of trusted parameters through.
      def matkul_params
        params.require(:matkul).permit(:kdmk, :mk, :sks)
      end
  end