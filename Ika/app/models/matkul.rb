class Matkul < ApplicationRecord
    has_many :nilais

    def new_attributes
        {
            id: self.id,
            kdmk: self.kdmk,
            mk: self.mk,
            sks: self.sks,
        }
    end
end
