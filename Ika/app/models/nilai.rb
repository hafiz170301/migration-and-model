class Nilai < ApplicationRecord
    belongs_to :mahasiswa
    belongs_to :matkul

    def new_attributes
        {
            id: self.id,
            nim: self.nim,
            kdmk: self.kdmk,
            nilai: self.nilai,
        }
    end
end
