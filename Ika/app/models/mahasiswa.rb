class Mahasiswa < ApplicationRecord
    has_many :nilais
    belongs_to :dosen

    def new_attributes
        {
            id: self.id,
            nim: self.nim,
            namaMhs: self.namaMhs,
            npm: self.npm,
        }
    end
end
