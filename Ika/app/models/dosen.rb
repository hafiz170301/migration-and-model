class Dosen < ApplicationRecord
    has_many :mahasiswas

    def new_attributes
        {
            id: self.id,
            npm: self.npm,
            namaDsn: self.namaDsn,
        }
    end
end
