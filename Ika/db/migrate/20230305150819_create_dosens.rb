class CreateDosens < ActiveRecord::Migration[7.0]
  def change
    create_table :dosens do |t|
      t.integer :npm
      t.string :namaDsn

      t.timestamps
    end
  end
end
