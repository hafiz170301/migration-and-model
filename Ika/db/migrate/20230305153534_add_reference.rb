class AddReference < ActiveRecord::Migration[7.0]
  def change
    add_reference :mahasiswas, :dosen, index: true
    add_reference :nilais, :mahasiswas, index:true
    add_reference :nilais, :matkul, index:true
  end
end
