class CreateNilais < ActiveRecord::Migration[7.0]
  def change
    create_table :nilais do |t|
      t.integer :nim
      t.string :kdmk
      t.string :nilai

      t.timestamps
    end
  end
end
