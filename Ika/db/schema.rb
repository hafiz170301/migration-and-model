# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_03_05_153534) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "dosens", force: :cascade do |t|
    t.integer "npm"
    t.string "namaDsn"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mahasiswas", force: :cascade do |t|
    t.integer "nim"
    t.string "namaMhs"
    t.integer "npm"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "dosen_id"
    t.index ["dosen_id"], name: "index_mahasiswas_on_dosen_id"
  end

  create_table "matkuls", force: :cascade do |t|
    t.string "kdmk"
    t.string "mk"
    t.integer "sks"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "nilais", force: :cascade do |t|
    t.integer "nim"
    t.string "kdmk"
    t.string "nilai"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "mahasiswas_id"
    t.bigint "matkul_id"
    t.index ["mahasiswas_id"], name: "index_nilais_on_mahasiswas_id"
    t.index ["matkul_id"], name: "index_nilais_on_matkul_id"
  end

end
