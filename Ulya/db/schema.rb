# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_03_06_084339) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ms_customers", force: :cascade do |t|
    t.string "customer_name"
    t.string "customer_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ms_products", force: :cascade do |t|
    t.string "product_name"
    t.decimal "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ms_staffs", force: :cascade do |t|
    t.string "staff_name"
    t.string "staff_position"
    t.text "staff_adress"
    t.string "staff_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transaction_details", force: :cascade do |t|
    t.integer "quality"
    t.bigint "transaction_header_id", null: false
    t.bigint "ms_product_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ms_product_id"], name: "index_transaction_details_on_ms_product_id"
    t.index ["transaction_header_id"], name: "index_transaction_details_on_transaction_header_id"
  end

  create_table "transaction_headers", force: :cascade do |t|
    t.date "sales_date"
    t.decimal "payment"
    t.decimal "change"
    t.bigint "ms_staff_id", null: false
    t.bigint "ms_customer_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ms_customer_id"], name: "index_transaction_headers_on_ms_customer_id"
    t.index ["ms_staff_id"], name: "index_transaction_headers_on_ms_staff_id"
  end

  add_foreign_key "transaction_details", "ms_products"
  add_foreign_key "transaction_details", "transaction_headers"
  add_foreign_key "transaction_headers", "ms_customers"
  add_foreign_key "transaction_headers", "ms_staffs"
end
