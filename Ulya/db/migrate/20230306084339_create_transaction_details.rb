class CreateTransactionDetails < ActiveRecord::Migration[7.0]
  def change
    create_table :transaction_details do |t|
      t.integer :quality
      t.references :transaction_header, null: false, foreign_key: true
      t.references :ms_product, null: false, foreign_key: true

      t.timestamps
    end
  end
end
