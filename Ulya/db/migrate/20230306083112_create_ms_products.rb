class CreateMsProducts < ActiveRecord::Migration[7.0]
  def change
    create_table :ms_products do |t|
      t.string :product_name
      t.decimal :price

      t.timestamps
    end
  end
end
