class CreateMsStaffs < ActiveRecord::Migration[7.0]
  def change
    create_table :ms_staffs do |t|
      t.string :staff_name
      t.string :staff_position
      t.text :staff_adress
      t.string :staff_email

      t.timestamps
    end
  end
end
