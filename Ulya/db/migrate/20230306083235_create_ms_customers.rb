class CreateMsCustomers < ActiveRecord::Migration[7.0]
  def change
    create_table :ms_customers do |t|
      t.string :customer_name
      t.string :customer_email

      t.timestamps
    end
  end
end
