class CreateTransactionHeaders < ActiveRecord::Migration[7.0]
  def change
    create_table :transaction_headers do |t|
      t.date :sales_date
      t.decimal :payment
      t.decimal :change
      t.references :ms_staff, null: false, foreign_key: true
      t.references :ms_customer, null: false, foreign_key: true

      t.timestamps
    end
  end
end
