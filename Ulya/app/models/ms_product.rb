class MsProduct < ApplicationRecord
    has_many: transaction_details
    has_many: transaction_headers, through: :transaction_details
end
