class TransactionDetail < ApplicationRecord
  belongs_to: transaction_header
  belongs_to: ms_product
end
