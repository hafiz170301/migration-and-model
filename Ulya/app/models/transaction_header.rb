class TransactionHeader < ApplicationRecord
  belongs_to: ms_staff
  belongs_to: ms_customer
  has_many: transaction_details
  has_many: ms_products, through: :transaction_details
end
