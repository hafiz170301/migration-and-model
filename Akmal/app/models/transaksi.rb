class Transaksi < ApplicationRecord
    belongs_to :pegawai
    belongs_to :barang
    enum :payment, [:cash, :qris, :debit]
end
