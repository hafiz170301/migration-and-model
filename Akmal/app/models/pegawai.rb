class Pegawai < ApplicationRecord
    has_many :transaksi
    enum :status, [:active, :not_active]
end
