json.extract! transaksi, :id, :name, :payment, :price, :qty, :created_at, :updated_at
json.url transaksi_url(transaksi, format: :json)
