json.extract! pegawai, :id, :name, :status, :created_at, :updated_at
json.url pegawai_url(pegawai, format: :json)
