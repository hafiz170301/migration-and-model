json.extract! barang, :id, :name, :harga, :qty, :created_at, :updated_at
json.url barang_url(barang, format: :json)
