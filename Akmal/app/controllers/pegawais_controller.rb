class PegawaisController < ApplicationController
  before_action :set_pegawai, only: %i[ show edit update destroy ]

  # GET /pegawais or /pegawais.json
  def index
    @pegawais = Pegawai.all
  end

  # GET /pegawais/1 or /pegawais/1.json
  def show
  end

  # GET /pegawais/new
  def new
    @pegawai = Pegawai.new
  end

  # GET /pegawais/1/edit
  def edit
  end

  # POST /pegawais or /pegawais.json
  def create
    @pegawai = Pegawai.new(pegawai_params)

    respond_to do |format|
      if @pegawai.save
        format.html { redirect_to pegawai_url(@pegawai), notice: "Pegawai was successfully created." }
        format.json { render :show, status: :created, location: @pegawai }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @pegawai.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pegawais/1 or /pegawais/1.json
  def update
    respond_to do |format|
      if @pegawai.update(pegawai_params)
        format.html { redirect_to pegawai_url(@pegawai), notice: "Pegawai was successfully updated." }
        format.json { render :show, status: :ok, location: @pegawai }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @pegawai.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pegawais/1 or /pegawais/1.json
  def destroy
    @pegawai.destroy

    respond_to do |format|
      format.html { redirect_to pegawais_url, notice: "Pegawai was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pegawai
      @pegawai = Pegawai.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def pegawai_params
      pp = params.require(:pegawai).permit(:name, :status)
      pp[:status] = params[:pegawai][:status].to_i
      return pp
    end
end
