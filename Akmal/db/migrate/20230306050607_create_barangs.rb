class CreateBarangs < ActiveRecord::Migration[7.0]
  def change
    create_table :barangs do |t|
      t.string :name
      t.float :harga
      t.integer :qty

      t.timestamps
    end
  end
end
