class CreateTransaksis < ActiveRecord::Migration[7.0]
  def change
    create_table :transaksis do |t|
      t.string :name
      t.integer :payment
      t.float :price
      t.integer :qty

      t.timestamps
    end
  end
end
