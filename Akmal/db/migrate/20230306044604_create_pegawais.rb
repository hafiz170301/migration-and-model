class CreatePegawais < ActiveRecord::Migration[7.0]
  def change
    create_table :pegawais do |t|
      t.string :name
      t.integer :status, default: 0
      #Ex:- :default =>''
      #Ex:- :default =>''

      t.timestamps
    end
  end
end
