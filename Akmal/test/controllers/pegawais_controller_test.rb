require "test_helper"

class PegawaisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pegawai = pegawais(:one)
  end

  test "should get index" do
    get pegawais_url
    assert_response :success
  end

  test "should get new" do
    get new_pegawai_url
    assert_response :success
  end

  test "should create pegawai" do
    assert_difference("Pegawai.count") do
      post pegawais_url, params: { pegawai: { name: @pegawai.name, status: @pegawai.status } }
    end

    assert_redirected_to pegawai_url(Pegawai.last)
  end

  test "should show pegawai" do
    get pegawai_url(@pegawai)
    assert_response :success
  end

  test "should get edit" do
    get edit_pegawai_url(@pegawai)
    assert_response :success
  end

  test "should update pegawai" do
    patch pegawai_url(@pegawai), params: { pegawai: { name: @pegawai.name, status: @pegawai.status } }
    assert_redirected_to pegawai_url(@pegawai)
  end

  test "should destroy pegawai" do
    assert_difference("Pegawai.count", -1) do
      delete pegawai_url(@pegawai)
    end

    assert_redirected_to pegawais_url
  end
end
