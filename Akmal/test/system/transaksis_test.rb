require "application_system_test_case"

class TransaksisTest < ApplicationSystemTestCase
  setup do
    @transaksi = transaksis(:one)
  end

  test "visiting the index" do
    visit transaksis_url
    assert_selector "h1", text: "Transaksis"
  end

  test "should create transaksi" do
    visit transaksis_url
    click_on "New transaksi"

    fill_in "Name", with: @transaksi.name
    fill_in "Payment", with: @transaksi.payment
    fill_in "Price", with: @transaksi.price
    fill_in "Qty", with: @transaksi.qty
    click_on "Create Transaksi"

    assert_text "Transaksi was successfully created"
    click_on "Back"
  end

  test "should update Transaksi" do
    visit transaksi_url(@transaksi)
    click_on "Edit this transaksi", match: :first

    fill_in "Name", with: @transaksi.name
    fill_in "Payment", with: @transaksi.payment
    fill_in "Price", with: @transaksi.price
    fill_in "Qty", with: @transaksi.qty
    click_on "Update Transaksi"

    assert_text "Transaksi was successfully updated"
    click_on "Back"
  end

  test "should destroy Transaksi" do
    visit transaksi_url(@transaksi)
    click_on "Destroy this transaksi", match: :first

    assert_text "Transaksi was successfully destroyed"
  end
end
