require "application_system_test_case"

class PegawaisTest < ApplicationSystemTestCase
  setup do
    @pegawai = pegawais(:one)
  end

  test "visiting the index" do
    visit pegawais_url
    assert_selector "h1", text: "Pegawais"
  end

  test "should create pegawai" do
    visit pegawais_url
    click_on "New pegawai"

    fill_in "Name", with: @pegawai.name
    fill_in "Status", with: @pegawai.status
    click_on "Create Pegawai"

    assert_text "Pegawai was successfully created"
    click_on "Back"
  end

  test "should update Pegawai" do
    visit pegawai_url(@pegawai)
    click_on "Edit this pegawai", match: :first

    fill_in "Name", with: @pegawai.name
    fill_in "Status", with: @pegawai.status
    click_on "Update Pegawai"

    assert_text "Pegawai was successfully updated"
    click_on "Back"
  end

  test "should destroy Pegawai" do
    visit pegawai_url(@pegawai)
    click_on "Destroy this pegawai", match: :first

    assert_text "Pegawai was successfully destroyed"
  end
end
