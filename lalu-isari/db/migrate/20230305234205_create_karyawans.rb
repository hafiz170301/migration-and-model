class CreateKaryawans < ActiveRecord::Migration[7.0]
  def change
    create_table :karyawans do |t|
      t.string :nama
      t.string :alamat
      t.integer :gaji
      t.references :bidang, null: false, foreign_key: true

      t.timestamps
    end
  end
end
