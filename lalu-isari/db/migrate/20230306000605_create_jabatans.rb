class CreateJabatans < ActiveRecord::Migration[7.0]
  def change
    create_table :jabatans do |t|
      t.string :nama
      t.references :bidang, null: false, foreign_key: true

      t.timestamps
    end
  end
end
