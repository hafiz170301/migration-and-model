class CreateBidangs < ActiveRecord::Migration[7.0]
  def change
    create_table :bidangs do |t|
      t.string :nama

      t.timestamps
    end
  end
end
