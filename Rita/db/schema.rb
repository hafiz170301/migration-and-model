# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_03_05_050052) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "dosens", force: :cascade do |t|
    t.integer "nip"
    t.string "nama_dosen"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "ruang_id"
    t.index ["ruang_id"], name: "index_dosens_on_ruang_id"
  end

  create_table "matakuliahs", force: :cascade do |t|
    t.string "kode_mk"
    t.string "nama_mk"
    t.integer "sks"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "ruang_id"
    t.bigint "tipe_mk_id"
    t.bigint "dosen_id"
    t.index ["dosen_id"], name: "index_matakuliahs_on_dosen_id"
    t.index ["ruang_id"], name: "index_matakuliahs_on_ruang_id"
    t.index ["tipe_mk_id"], name: "index_matakuliahs_on_tipe_mk_id"
  end

  create_table "ruangs", force: :cascade do |t|
    t.string "kode_mk"
    t.string "nama_mk"
    t.string "kelas"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipe_mks", force: :cascade do |t|
    t.string "nama_mk"
    t.string "jenis_mk"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
