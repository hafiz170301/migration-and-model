class AddRuangIdToDosen < ActiveRecord::Migration[7.0]
  def change
    add_reference :dosens, :ruang, index: true
  end
end
