class AddForeignKeysToMatakuliah < ActiveRecord::Migration[7.0]
  def change
    add_reference :matakuliahs, :ruang, index: true
    add_reference :matakuliahs, :tipe_mk, index: true
    add_reference :matakuliahs, :dosen,  index: true

  end
end
