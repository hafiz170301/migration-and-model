class CreateRuangs < ActiveRecord::Migration[7.0]
  def change
    create_table :ruangs do |t|
      t.string :kode_mk
      t.string :nama_mk
      t.string :kelas

      t.timestamps
    end
  end
end
