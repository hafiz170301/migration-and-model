class CreateTipeMks < ActiveRecord::Migration[7.0]
  def change
    create_table :tipe_mks do |t|
      t.string :nama_mk
      t.string :jenis_mk

      t.timestamps
    end
  end
end
