class CreateMatakuliahs < ActiveRecord::Migration[7.0]
  def change
    create_table :matakuliahs do |t|
      t.string :kode_mk
      t.string :nama_mk
      t.integer :sks

      t.timestamps
    end
  end
end
