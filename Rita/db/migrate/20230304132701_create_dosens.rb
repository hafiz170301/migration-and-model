class CreateDosens < ActiveRecord::Migration[7.0]
  def change
    create_table :dosens do |t|
      t.integer :nip
      t.string :nama_dosen

      t.timestamps
    end
  end
end
