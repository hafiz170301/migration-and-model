class Matakuliah < ApplicationRecord
    belongs_to :dosen
    def new_attributes
        {
          kode_mk: self.kode_mk,
          nama_mk: self.nama_mk,
          sks: self.sks,
          created_at: self.created_at,
        }
      end
end
