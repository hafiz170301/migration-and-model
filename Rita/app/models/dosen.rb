class Dosen < ApplicationRecord
    has_many :matakuliahs
    has_many :ruangs

    def new_attributes
        {
          nip: self.nip,
          nama_dosen: self.nama_dosen,
          created_at: self.created_at,
        }
    end
end
