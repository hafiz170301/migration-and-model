class Ruang < ApplicationRecord
  has_many :matakuliahs
  belongs_to :dosen

    def new_attributes
        {
          kode_mk: self.kode_mk,
          nama_mk: self.nama_mk,
          kelas : self.kelas,
          created_at: self.created_at,
        }
    end
end
