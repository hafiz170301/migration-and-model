class CreateMahasiswas < ActiveRecord::Migration[7.0]
  def change
    create_table :mahasiswas do |t|
      t.string :name
      t.references :dosen, null: false, foreign_key: true

      t.timestamps
    end
  end
end
