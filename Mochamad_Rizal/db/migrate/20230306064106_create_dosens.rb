class CreateDosens < ActiveRecord::Migration[7.0]
  def change
    create_table :dosens do |t|
      t.string :name

      t.timestamps
    end
  end
end
