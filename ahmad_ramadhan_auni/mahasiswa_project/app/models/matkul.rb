class Matkul < ApplicationRecord
    has_many :mahasiswas_matkuls
    has_many :mahasiswas, through: :mahasiswas_matkuls
end
