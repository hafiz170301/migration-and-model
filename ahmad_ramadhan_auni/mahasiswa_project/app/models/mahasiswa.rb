class Mahasiswa < ApplicationRecord
    belongs_to :askot
    
    has_many :mahasiswas_matkuls
    has_many :matkuls, through: :mahasiswas_matkuls

    def coba
        {
            "nim": self.nim,
            "nama": self.nama,
            "alamat": self.alamat,
            "askot_id": self.askot
        }
    end

end 
