class MahasiswasController < ApplicationController
    def index
        @mahasiswas = Mahasiswa.all

        render json: @mahasiswas
    end

    def show
        @mahasiswa = Mahasiswa.find(params[:id])

        render json: @mahasiswa
    end

    def create
        @mahasiswa = Mahasiswa.new(nim: params[:nim], nama: params[:nama], alamat: params[:alamat])
    
        render json: "#{@mahasiswa.nama} has been created!"
    end

    def update
        @mahasiswa = Mahasiswa.find(params[:id])
        @mahasiswa.update(nim: params[:nim], nama: params[:nama], alamat: params[:alamat])

        render json: "#{mahasiswa.nama} has been updated!"
    end

    def destroy
        @mahasiswa = Mahasiswa.find(params[:id])
        @mahasiswa.destroy

        render json: "#{mahasiswa.nama} has been deleted!"
    end
end
