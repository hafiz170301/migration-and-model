class MatkulsController < ApplicationController
    def index
        @matkuls = Matkul.all

        render json: @matkuls
    end

    def show
        @matkul = Matkul.find(params[:id])

        render json: @matkul
    end

    def create
        @matkul = Matkul.new(nama: params[:nama], sks: params[:sks])

        render json: "#{@matkul.nama} has been created!"
    end

    def update
        @matkul = Matkul.find(params[:id])
        @matkul.update(nama: params[:nama], sks: params[:sks])

        render json: "#{matkul.nama} has been updated"
    end

    def destroy
        @matkul = Matkul.find(params[:id])
        @matkul.destroy

        render json: "#{matkul.nama} has been deleted!"
    end
end
