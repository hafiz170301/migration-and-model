class AskotsController < ApplicationController
    def index
        @askots = Askot.all

        render json: @askots
    end

    def show
        @askot = Askot.find(params[:id])

        render json: @askot
    end

    def create
        @askot = Askot.new(nama: params[:nama])

        render json: "#{@askot.nama} has been created!"

    end

    def update
        @askot = Askot.find(params[:id])
        @askot.update(nama: params[:nama])

        render json: "#{@askot.nama} has been updated!"
    end
    
    def destroy
        @askot = Askot.find(params[:id])
        @askot.destroy

        render json: "#{askot.nama} has been deleted!"
    end
end