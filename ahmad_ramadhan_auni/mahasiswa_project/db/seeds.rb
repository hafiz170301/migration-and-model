# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
Askot.destroy_all
Matkul.destroy_all
Mahasiswa.destroy_all

askot1 = Askot.create(nama: "Jakarta")
askot2 = Askot.create(nama: "Semarang")
askot3 = Askot.create(nama: "Yogyakarta")

matkul1 = Matkul.create(nama: "Statistika", sks: 3)
matkul1 = Matkul.create(nama: "Pemrograman Web", sks: 3)
matkul1 = Matkul.create(nama: "Struktur Data", sks: 2)

mahasiswa1 = Mahasiswa.create(nim: 001, nama: "Udin", alamat: "Gang Udang", askot_id: 1)
mahasiswa2 = Mahasiswa.create(nim: 002, nama: "Sari", alamat: "Gang wader", askot_id: 2)
mahasiswa3 = Mahasiswa.create(nim: 003, nama: "Ujang", alamat: "jalan baru", askot_id: 3)
mahasiswa4 = Mahasiswa.create(nim: 004, nama: "Widodo", alamat: "Gang utama", askot_id: 3)
mahasiswa5 = Mahasiswa.create(nim: 005, nama: "Agus", alamat: "Jalan sarirejo", askot_id: 2)
mahasiswa6 = Mahasiswa.create(nim: 006, nama: "Zidane", alamat: "Gang besar", askot_id: 1)
mahasiswa7 = Mahasiswa.create(nim: 007, nama: "Alul", alamat: "Gang kecil", askot_id: 1)

