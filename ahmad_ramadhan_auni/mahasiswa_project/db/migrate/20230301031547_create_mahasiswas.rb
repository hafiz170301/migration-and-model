class CreateMahasiswas < ActiveRecord::Migration[7.0]
  def change
    create_table :mahasiswas do |t|
      t.integer :nim
      t.string :nama
      t.string :alamat

      t.timestamps
    end
  end
end
