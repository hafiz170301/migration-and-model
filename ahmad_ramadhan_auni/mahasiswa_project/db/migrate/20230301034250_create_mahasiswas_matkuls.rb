class CreateMahasiswasMatkuls < ActiveRecord::Migration[7.0]
  def change
    create_table :mahasiswas_matkuls do |t|
      t.integer :mahasiswa_id
      t.integer :matkul_id
    end
  end
end
