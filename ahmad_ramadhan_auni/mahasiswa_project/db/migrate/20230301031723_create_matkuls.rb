class CreateMatkuls < ActiveRecord::Migration[7.0]
  def change
    create_table :matkuls do |t|
      t.string :nama
      t.integer :sks

      t.timestamps
    end
  end
end
