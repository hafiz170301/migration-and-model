class CreateBiayas < ActiveRecord::Migration[7.0]
  def change
    create_table :biayas do |t|
      t.string :jenis_hari
      t.string :jenis_lapangan
      t.time :jam
      t.integer :biaya

      t.timestamps
    end
  end
end
