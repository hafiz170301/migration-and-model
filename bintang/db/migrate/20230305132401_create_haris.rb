class CreateHaris < ActiveRecord::Migration[7.0]
  def change
    create_table :haris do |t|
      t.string :nama_hari_sewa
      t.string :jenis_hari

      t.timestamps
    end
  end
end
