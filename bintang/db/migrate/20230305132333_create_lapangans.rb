class CreateLapangans < ActiveRecord::Migration[7.0]
  def change
    create_table :lapangans do |t|
      t.string :nama_lapangan
      t.string :jenis_lapangan

      t.timestamps
    end
  end
end
