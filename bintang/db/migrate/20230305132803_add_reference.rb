class AddReference < ActiveRecord::Migration[7.0]
  def change
    add_reference :penyewaans, :customer, index: true
    add_reference :penyewaans, :pegawai, index: true
  end
end
