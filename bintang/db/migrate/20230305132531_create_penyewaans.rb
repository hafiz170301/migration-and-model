class CreatePenyewaans < ActiveRecord::Migration[7.0]
  def change
    create_table :penyewaans do |t|
      t.date :tgl_sewa
      t.date :tgl_pakai
      t.time :jam_sewa
      t.string :hari_sewa
      t.string :lapangan
      t.integer :biaya_sewa

      t.timestamps
    end
  end
end
