class CreateCustomers < ActiveRecord::Migration[7.0]
  def change
    create_table :customers do |t|
      t.string :nama_customer

      t.timestamps
    end
  end
end
