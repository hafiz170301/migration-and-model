# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_03_05_132803) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "biayas", force: :cascade do |t|
    t.string "jenis_hari"
    t.string "jenis_lapangan"
    t.time "jam"
    t.integer "biaya"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customers", force: :cascade do |t|
    t.string "nama_customer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "haris", force: :cascade do |t|
    t.string "nama_hari_sewa"
    t.string "jenis_hari"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lapangans", force: :cascade do |t|
    t.string "nama_lapangan"
    t.string "jenis_lapangan"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pegawais", force: :cascade do |t|
    t.string "nama_pegawai"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "penyewaans", force: :cascade do |t|
    t.date "tgl_sewa"
    t.date "tgl_pakai"
    t.time "jam_sewa"
    t.string "hari_sewa"
    t.string "lapangan"
    t.integer "biaya_sewa"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "customer_id"
    t.bigint "pegawai_id"
    t.index ["customer_id"], name: "index_penyewaans_on_customer_id"
    t.index ["pegawai_id"], name: "index_penyewaans_on_pegawai_id"
  end

end
