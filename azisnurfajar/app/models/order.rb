class Order < ApplicationRecord
  belongs_to :customer
  belongs_to :barang
  has_many :barangs
end
