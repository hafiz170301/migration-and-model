class Barang < ApplicationRecord
  belongs_to :supplier
  belongs_to :penjual
  has_many :orders
end
