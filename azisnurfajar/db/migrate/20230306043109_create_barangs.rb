class CreateBarangs < ActiveRecord::Migration[7.0]
  def change
    create_table :barangs do |t|
      t.string :nama_barang
      t.integer :harga
      t.integer :stok
      t.references :supplier, null: false, foreign_key: true
      t.references :penjual, null: false, foreign_key: true

      t.timestamps
    end
  end
end
