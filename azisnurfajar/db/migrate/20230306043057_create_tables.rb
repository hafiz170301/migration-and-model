class CreateTables < ActiveRecord::Migration[7.0]
  def change
    create_table :tables do |t|
      t.string :kurir
      t.string :nama_kurir
      t.text :alamat

      t.timestamps
    end
  end
end
