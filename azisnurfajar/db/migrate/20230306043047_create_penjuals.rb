class CreatePenjuals < ActiveRecord::Migration[7.0]
  def change
    create_table :penjuals do |t|
      t.string :nama_penjual
      t.text :alamat

      t.timestamps
    end
  end
end
