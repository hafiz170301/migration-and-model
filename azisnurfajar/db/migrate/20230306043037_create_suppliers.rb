class CreateSuppliers < ActiveRecord::Migration[7.0]
  def change
    create_table :suppliers do |t|
      t.string :nama_suplier
      t.text :alamat

      t.timestamps
    end
  end
end
