class CreateKurirs < ActiveRecord::Migration[7.0]
  def change
    create_table :kurirs do |t|
      t.string :nama_kurir
      t.text :alamat

      t.timestamps
    end
  end
end
