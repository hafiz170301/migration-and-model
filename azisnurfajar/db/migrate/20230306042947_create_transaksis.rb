class CreateTransaksis < ActiveRecord::Migration[7.0]
  def change
    create_table :transaksis do |t|
      t.string :nama_bank
      t.string :kode_pembayaran
      t.references :pengguna, null: false, foreign_key: true

      t.timestamps
    end
  end
end
