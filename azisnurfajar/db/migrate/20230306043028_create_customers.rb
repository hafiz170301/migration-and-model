class CreateCustomers < ActiveRecord::Migration[7.0]
  def change
    create_table :customers do |t|
      t.integer :nik
      t.string :nama_customer
      t.string :jenis_kelamin
      t.string :no_hp
      t.text :alamat
      t.references :pengguna, null: false, foreign_key: true

      t.timestamps
    end
  end
end
