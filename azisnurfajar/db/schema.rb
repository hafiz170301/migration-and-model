# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_03_06_065419) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "barangs", force: :cascade do |t|
    t.string "nama_barang"
    t.integer "harga"
    t.integer "stok"
    t.bigint "supplier_id", null: false
    t.bigint "penjual_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["penjual_id"], name: "index_barangs_on_penjual_id"
    t.index ["supplier_id"], name: "index_barangs_on_supplier_id"
  end

  create_table "customers", force: :cascade do |t|
    t.integer "nik"
    t.string "nama_customer"
    t.string "jenis_kelamin"
    t.string "no_hp"
    t.text "alamat"
    t.bigint "pengguna_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pengguna_id"], name: "index_customers_on_pengguna_id"
  end

  create_table "kurirs", force: :cascade do |t|
    t.string "nama_kurir"
    t.text "alamat"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.string "status"
    t.bigint "customer_id", null: false
    t.bigint "barang_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["barang_id"], name: "index_orders_on_barang_id"
    t.index ["customer_id"], name: "index_orders_on_customer_id"
  end

  create_table "penggunas", force: :cascade do |t|
    t.string "email"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "penjuals", force: :cascade do |t|
    t.string "nama_penjual"
    t.text "alamat"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "suppliers", force: :cascade do |t|
    t.string "nama_suplier"
    t.text "alamat"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tables", force: :cascade do |t|
    t.string "kurir"
    t.string "nama_kurir"
    t.text "alamat"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transaksis", force: :cascade do |t|
    t.string "nama_bank"
    t.string "kode_pembayaran"
    t.bigint "pengguna_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pengguna_id"], name: "index_transaksis_on_pengguna_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "barangs", "penjuals"
  add_foreign_key "barangs", "suppliers"
  add_foreign_key "customers", "penggunas"
  add_foreign_key "orders", "barangs"
  add_foreign_key "orders", "customers"
  add_foreign_key "transaksis", "penggunas"
end
