class Kendaraan < ApplicationRecord
    belongs_to :superviser
    has_many :pelanggarans

    def new_attribut
        {
            id: self.id,
            nmr_kendaraan: self.nmr_kendaraan,
            warna: self.warna,
            tipe: self.tipe,
            tahun: self.tahun,
            nmr_lambung: self.nmr_lambung,
        }
    end
end
