class Pelanggaran < ApplicationRecord

    belongs_to :kendaraan
    belongs_to :sopir

    def new_attribut
        {
        id: self.id,
        nmr_nota: self.nmr_nota,
        tgl_pelanggaran: self.tgl_pelanggaran,
        lokasi: self.lokasi,
        cttn_pelanggaran: self.cttn_pelanggaran,
        tindakan: self.tindakan
        }
    end
end
