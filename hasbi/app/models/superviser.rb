class Superviser < ApplicationRecord

    has_many :kendaraans
    
    def new_attribut
        {
            id: self.id,
            area_spv: self.area_spv,
        }
    end
end
