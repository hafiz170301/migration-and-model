class Sopir < ApplicationRecord
    has_many :pelanggarans

    def new_attribut
        {
            id: self.id,
            sim: self.sim,
            nama_sopir: self.nama_sopir,
            alamat: self.alamat,
            kota: self.kota,
            kd_pos: self.kd_pos,
            jk: self.jk,
            tb: self.tb,
            bb: self.bb,
        }
    end
end
