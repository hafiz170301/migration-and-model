class TabelPelanggaransController < ApplicationController
  before_action :set_tabel_pelanggaran, only: %i[ show edit update destroy ]

  # GET /tabel_pelanggarans or /tabel_pelanggarans.json
  def index
    @tabel_pelanggarans = TabelPelanggaran.all
  end

  # GET /tabel_pelanggarans/1 or /tabel_pelanggarans/1.json
  def show
  end

  # GET /tabel_pelanggarans/new
  def new
    @tabel_pelanggaran = TabelPelanggaran.new
  end

  # GET /tabel_pelanggarans/1/edit
  def edit
  end

  # POST /tabel_pelanggarans or /tabel_pelanggarans.json
  def create
    @tabel_pelanggaran = TabelPelanggaran.new(tabel_pelanggaran_params)

    respond_to do |format|
      if @tabel_pelanggaran.save
        format.html { redirect_to tabel_pelanggaran_url(@tabel_pelanggaran), notice: "Tabel pelanggaran was successfully created." }
        format.json { render :show, status: :created, location: @tabel_pelanggaran }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @tabel_pelanggaran.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tabel_pelanggarans/1 or /tabel_pelanggarans/1.json
  def update
    respond_to do |format|
      if @tabel_pelanggaran.update(tabel_pelanggaran_params)
        format.html { redirect_to tabel_pelanggaran_url(@tabel_pelanggaran), notice: "Tabel pelanggaran was successfully updated." }
        format.json { render :show, status: :ok, location: @tabel_pelanggaran }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @tabel_pelanggaran.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tabel_pelanggarans/1 or /tabel_pelanggarans/1.json
  def destroy
    @tabel_pelanggaran.destroy

    respond_to do |format|
      format.html { redirect_to tabel_pelanggarans_url, notice: "Tabel pelanggaran was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tabel_pelanggaran
      @tabel_pelanggaran = TabelPelanggaran.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def tabel_pelanggaran_params
      params.require(:tabel_pelanggaran).permit(:ID_Nota, :No_Nota, :Tgl_Pelanggaran, :Lokasi, :Pelanggaran, :Tindakan)
    end
end
