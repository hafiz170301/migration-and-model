class TabelSopirsController < ApplicationController
  before_action :set_tabel_sopir, only: %i[ show edit update destroy ]

  # GET /tabel_sopirs or /tabel_sopirs.json
  def index
    @tabel_sopirs = TabelSopir.all
  end

  # GET /tabel_sopirs/1 or /tabel_sopirs/1.json
  def show
  end

  # GET /tabel_sopirs/new
  def new
    @tabel_sopir = TabelSopir.new
  end

  # GET /tabel_sopirs/1/edit
  def edit
  end

  # POST /tabel_sopirs or /tabel_sopirs.json
  def create
    @tabel_sopir = TabelSopir.new(tabel_sopir_params)

    respond_to do |format|
      if @tabel_sopir.save
        format.html { redirect_to tabel_sopir_url(@tabel_sopir), notice: "Tabel sopir was successfully created." }
        format.json { render :show, status: :created, location: @tabel_sopir }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @tabel_sopir.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tabel_sopirs/1 or /tabel_sopirs/1.json
  def update
    respond_to do |format|
      if @tabel_sopir.update(tabel_sopir_params)
        format.html { redirect_to tabel_sopir_url(@tabel_sopir), notice: "Tabel sopir was successfully updated." }
        format.json { render :show, status: :ok, location: @tabel_sopir }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @tabel_sopir.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tabel_sopirs/1 or /tabel_sopirs/1.json
  def destroy
    @tabel_sopir.destroy

    respond_to do |format|
      format.html { redirect_to tabel_sopirs_url, notice: "Tabel sopir was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tabel_sopir
      @tabel_sopir = TabelSopir.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def tabel_sopir_params
      params.require(:tabel_sopir).permit(:SIM, :namaSopir, :JK, :Kota, :Provinsi, :TglLhr, :TB, :BB)
    end
end
