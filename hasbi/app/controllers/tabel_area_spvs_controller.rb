class TabelAreaSpvsController < ApplicationController
  before_action :set_tabel_area_spv, only: %i[ show edit update destroy ]

  # GET /tabel_area_spvs or /tabel_area_spvs.json
  def index
    @tabel_area_spvs = TabelAreaSpv.all
  end

  # GET /tabel_area_spvs/1 or /tabel_area_spvs/1.json
  def show
  end

  # GET /tabel_area_spvs/new
  def new
    @tabel_area_spv = TabelAreaSpv.new
  end

  # GET /tabel_area_spvs/1/edit
  def edit
  end

  # POST /tabel_area_spvs or /tabel_area_spvs.json
  def create
    @tabel_area_spv = TabelAreaSpv.new(tabel_area_spv_params)

    respond_to do |format|
      if @tabel_area_spv.save
        format.html { redirect_to tabel_area_spv_url(@tabel_area_spv), notice: "Tabel area spv was successfully created." }
        format.json { render :show, status: :created, location: @tabel_area_spv }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @tabel_area_spv.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tabel_area_spvs/1 or /tabel_area_spvs/1.json
  def update
    respond_to do |format|
      if @tabel_area_spv.update(tabel_area_spv_params)
        format.html { redirect_to tabel_area_spv_url(@tabel_area_spv), notice: "Tabel area spv was successfully updated." }
        format.json { render :show, status: :ok, location: @tabel_area_spv }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @tabel_area_spv.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tabel_area_spvs/1 or /tabel_area_spvs/1.json
  def destroy
    @tabel_area_spv.destroy

    respond_to do |format|
      format.html { redirect_to tabel_area_spvs_url, notice: "Tabel area spv was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tabel_area_spv
      @tabel_area_spv = TabelAreaSpv.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def tabel_area_spv_params
      params.require(:tabel_area_spv).permit(:ID_Area, :Area)
    end
end
