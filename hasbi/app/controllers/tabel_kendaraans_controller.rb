class TabelKendaraansController < ApplicationController
  before_action :set_tabel_kendaraan, only: %i[ show edit update destroy ]

  # GET /tabel_kendaraans or /tabel_kendaraans.json
  def index
    @tabel_kendaraans = TabelKendaraan.all
  end

  # GET /tabel_kendaraans/1 or /tabel_kendaraans/1.json
  def show
  end

  # GET /tabel_kendaraans/new
  def new
    @tabel_kendaraan = TabelKendaraan.new
  end

  # GET /tabel_kendaraans/1/edit
  def edit
  end

  # POST /tabel_kendaraans or /tabel_kendaraans.json
  def create
    @tabel_kendaraan = TabelKendaraan.new(tabel_kendaraan_params)

    respond_to do |format|
      if @tabel_kendaraan.save
        format.html { redirect_to tabel_kendaraan_url(@tabel_kendaraan), notice: "Tabel kendaraan was successfully created." }
        format.json { render :show, status: :created, location: @tabel_kendaraan }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @tabel_kendaraan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tabel_kendaraans/1 or /tabel_kendaraans/1.json
  def update
    respond_to do |format|
      if @tabel_kendaraan.update(tabel_kendaraan_params)
        format.html { redirect_to tabel_kendaraan_url(@tabel_kendaraan), notice: "Tabel kendaraan was successfully updated." }
        format.json { render :show, status: :ok, location: @tabel_kendaraan }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @tabel_kendaraan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tabel_kendaraans/1 or /tabel_kendaraans/1.json
  def destroy
    @tabel_kendaraan.destroy

    respond_to do |format|
      format.html { redirect_to tabel_kendaraans_url, notice: "Tabel kendaraan was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tabel_kendaraan
      @tabel_kendaraan = TabelKendaraan.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def tabel_kendaraan_params
      params.require(:tabel_kendaraan).permit(:ID_Kendaraan, :No_Kndrn, :Warna, :No_Lambung)
    end
end
