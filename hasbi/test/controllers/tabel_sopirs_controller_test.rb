require "test_helper"

class TabelSopirsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tabel_sopir = tabel_sopirs(:one)
  end

  test "should get index" do
    get tabel_sopirs_url
    assert_response :success
  end

  test "should get new" do
    get new_tabel_sopir_url
    assert_response :success
  end

  test "should create tabel_sopir" do
    assert_difference("TabelSopir.count") do
      post tabel_sopirs_url, params: { tabel_sopir: { BB: @tabel_sopir.BB, JK: @tabel_sopir.JK, Kota: @tabel_sopir.Kota, Provinsi: @tabel_sopir.Provinsi, SIM: @tabel_sopir.SIM, TB: @tabel_sopir.TB, TglLhr: @tabel_sopir.TglLhr, namaSopir: @tabel_sopir.namaSopir } }
    end

    assert_redirected_to tabel_sopir_url(TabelSopir.last)
  end

  test "should show tabel_sopir" do
    get tabel_sopir_url(@tabel_sopir)
    assert_response :success
  end

  test "should get edit" do
    get edit_tabel_sopir_url(@tabel_sopir)
    assert_response :success
  end

  test "should update tabel_sopir" do
    patch tabel_sopir_url(@tabel_sopir), params: { tabel_sopir: { BB: @tabel_sopir.BB, JK: @tabel_sopir.JK, Kota: @tabel_sopir.Kota, Provinsi: @tabel_sopir.Provinsi, SIM: @tabel_sopir.SIM, TB: @tabel_sopir.TB, TglLhr: @tabel_sopir.TglLhr, namaSopir: @tabel_sopir.namaSopir } }
    assert_redirected_to tabel_sopir_url(@tabel_sopir)
  end

  test "should destroy tabel_sopir" do
    assert_difference("TabelSopir.count", -1) do
      delete tabel_sopir_url(@tabel_sopir)
    end

    assert_redirected_to tabel_sopirs_url
  end
end
