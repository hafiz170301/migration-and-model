require "test_helper"

class TabelKendaraansControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tabel_kendaraan = tabel_kendaraans(:one)
  end

  test "should get index" do
    get tabel_kendaraans_url
    assert_response :success
  end

  test "should get new" do
    get new_tabel_kendaraan_url
    assert_response :success
  end

  test "should create tabel_kendaraan" do
    assert_difference("TabelKendaraan.count") do
      post tabel_kendaraans_url, params: { tabel_kendaraan: { ID_Kendaraan: @tabel_kendaraan.ID_Kendaraan, No_Kndrn: @tabel_kendaraan.No_Kndrn, No_Lambung: @tabel_kendaraan.No_Lambung, Warna: @tabel_kendaraan.Warna } }
    end

    assert_redirected_to tabel_kendaraan_url(TabelKendaraan.last)
  end

  test "should show tabel_kendaraan" do
    get tabel_kendaraan_url(@tabel_kendaraan)
    assert_response :success
  end

  test "should get edit" do
    get edit_tabel_kendaraan_url(@tabel_kendaraan)
    assert_response :success
  end

  test "should update tabel_kendaraan" do
    patch tabel_kendaraan_url(@tabel_kendaraan), params: { tabel_kendaraan: { ID_Kendaraan: @tabel_kendaraan.ID_Kendaraan, No_Kndrn: @tabel_kendaraan.No_Kndrn, No_Lambung: @tabel_kendaraan.No_Lambung, Warna: @tabel_kendaraan.Warna } }
    assert_redirected_to tabel_kendaraan_url(@tabel_kendaraan)
  end

  test "should destroy tabel_kendaraan" do
    assert_difference("TabelKendaraan.count", -1) do
      delete tabel_kendaraan_url(@tabel_kendaraan)
    end

    assert_redirected_to tabel_kendaraans_url
  end
end
