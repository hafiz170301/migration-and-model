require "test_helper"

class TabelPelanggaransControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tabel_pelanggaran = tabel_pelanggarans(:one)
  end

  test "should get index" do
    get tabel_pelanggarans_url
    assert_response :success
  end

  test "should get new" do
    get new_tabel_pelanggaran_url
    assert_response :success
  end

  test "should create tabel_pelanggaran" do
    assert_difference("TabelPelanggaran.count") do
      post tabel_pelanggarans_url, params: { tabel_pelanggaran: { ID_Nota: @tabel_pelanggaran.ID_Nota, Lokasi: @tabel_pelanggaran.Lokasi, No_Nota: @tabel_pelanggaran.No_Nota, Pelanggaran: @tabel_pelanggaran.Pelanggaran, Tgl_Pelanggaran: @tabel_pelanggaran.Tgl_Pelanggaran, Tindakan: @tabel_pelanggaran.Tindakan } }
    end

    assert_redirected_to tabel_pelanggaran_url(TabelPelanggaran.last)
  end

  test "should show tabel_pelanggaran" do
    get tabel_pelanggaran_url(@tabel_pelanggaran)
    assert_response :success
  end

  test "should get edit" do
    get edit_tabel_pelanggaran_url(@tabel_pelanggaran)
    assert_response :success
  end

  test "should update tabel_pelanggaran" do
    patch tabel_pelanggaran_url(@tabel_pelanggaran), params: { tabel_pelanggaran: { ID_Nota: @tabel_pelanggaran.ID_Nota, Lokasi: @tabel_pelanggaran.Lokasi, No_Nota: @tabel_pelanggaran.No_Nota, Pelanggaran: @tabel_pelanggaran.Pelanggaran, Tgl_Pelanggaran: @tabel_pelanggaran.Tgl_Pelanggaran, Tindakan: @tabel_pelanggaran.Tindakan } }
    assert_redirected_to tabel_pelanggaran_url(@tabel_pelanggaran)
  end

  test "should destroy tabel_pelanggaran" do
    assert_difference("TabelPelanggaran.count", -1) do
      delete tabel_pelanggaran_url(@tabel_pelanggaran)
    end

    assert_redirected_to tabel_pelanggarans_url
  end
end
