require "test_helper"

class TabelAreaSpvsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tabel_area_spv = tabel_area_spvs(:one)
  end

  test "should get index" do
    get tabel_area_spvs_url
    assert_response :success
  end

  test "should get new" do
    get new_tabel_area_spv_url
    assert_response :success
  end

  test "should create tabel_area_spv" do
    assert_difference("TabelAreaSpv.count") do
      post tabel_area_spvs_url, params: { tabel_area_spv: { Area: @tabel_area_spv.Area, ID_Area: @tabel_area_spv.ID_Area } }
    end

    assert_redirected_to tabel_area_spv_url(TabelAreaSpv.last)
  end

  test "should show tabel_area_spv" do
    get tabel_area_spv_url(@tabel_area_spv)
    assert_response :success
  end

  test "should get edit" do
    get edit_tabel_area_spv_url(@tabel_area_spv)
    assert_response :success
  end

  test "should update tabel_area_spv" do
    patch tabel_area_spv_url(@tabel_area_spv), params: { tabel_area_spv: { Area: @tabel_area_spv.Area, ID_Area: @tabel_area_spv.ID_Area } }
    assert_redirected_to tabel_area_spv_url(@tabel_area_spv)
  end

  test "should destroy tabel_area_spv" do
    assert_difference("TabelAreaSpv.count", -1) do
      delete tabel_area_spv_url(@tabel_area_spv)
    end

    assert_redirected_to tabel_area_spvs_url
  end
end
