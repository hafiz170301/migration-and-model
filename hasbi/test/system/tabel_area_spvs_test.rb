require "application_system_test_case"

class TabelAreaSpvsTest < ApplicationSystemTestCase
  setup do
    @tabel_area_spv = tabel_area_spvs(:one)
  end

  test "visiting the index" do
    visit tabel_area_spvs_url
    assert_selector "h1", text: "Tabel area spvs"
  end

  test "should create tabel area spv" do
    visit tabel_area_spvs_url
    click_on "New tabel area spv"

    fill_in "Area", with: @tabel_area_spv.Area
    fill_in "Id area", with: @tabel_area_spv.ID_Area
    click_on "Create Tabel area spv"

    assert_text "Tabel area spv was successfully created"
    click_on "Back"
  end

  test "should update Tabel area spv" do
    visit tabel_area_spv_url(@tabel_area_spv)
    click_on "Edit this tabel area spv", match: :first

    fill_in "Area", with: @tabel_area_spv.Area
    fill_in "Id area", with: @tabel_area_spv.ID_Area
    click_on "Update Tabel area spv"

    assert_text "Tabel area spv was successfully updated"
    click_on "Back"
  end

  test "should destroy Tabel area spv" do
    visit tabel_area_spv_url(@tabel_area_spv)
    click_on "Destroy this tabel area spv", match: :first

    assert_text "Tabel area spv was successfully destroyed"
  end
end
