require "application_system_test_case"

class TabelSopirsTest < ApplicationSystemTestCase
  setup do
    @tabel_sopir = tabel_sopirs(:one)
  end

  test "visiting the index" do
    visit tabel_sopirs_url
    assert_selector "h1", text: "Tabel sopirs"
  end

  test "should create tabel sopir" do
    visit tabel_sopirs_url
    click_on "New tabel sopir"

    fill_in "Bb", with: @tabel_sopir.BB
    fill_in "Jk", with: @tabel_sopir.JK
    fill_in "Kota", with: @tabel_sopir.Kota
    fill_in "Provinsi", with: @tabel_sopir.Provinsi
    fill_in "Sim", with: @tabel_sopir.SIM
    fill_in "Tb", with: @tabel_sopir.TB
    fill_in "Tgllhr", with: @tabel_sopir.TglLhr
    fill_in "Namasopir", with: @tabel_sopir.namaSopir
    click_on "Create Tabel sopir"

    assert_text "Tabel sopir was successfully created"
    click_on "Back"
  end

  test "should update Tabel sopir" do
    visit tabel_sopir_url(@tabel_sopir)
    click_on "Edit this tabel sopir", match: :first

    fill_in "Bb", with: @tabel_sopir.BB
    fill_in "Jk", with: @tabel_sopir.JK
    fill_in "Kota", with: @tabel_sopir.Kota
    fill_in "Provinsi", with: @tabel_sopir.Provinsi
    fill_in "Sim", with: @tabel_sopir.SIM
    fill_in "Tb", with: @tabel_sopir.TB
    fill_in "Tgllhr", with: @tabel_sopir.TglLhr
    fill_in "Namasopir", with: @tabel_sopir.namaSopir
    click_on "Update Tabel sopir"

    assert_text "Tabel sopir was successfully updated"
    click_on "Back"
  end

  test "should destroy Tabel sopir" do
    visit tabel_sopir_url(@tabel_sopir)
    click_on "Destroy this tabel sopir", match: :first

    assert_text "Tabel sopir was successfully destroyed"
  end
end
