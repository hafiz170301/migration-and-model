require "application_system_test_case"

class TabelKendaraansTest < ApplicationSystemTestCase
  setup do
    @tabel_kendaraan = tabel_kendaraans(:one)
  end

  test "visiting the index" do
    visit tabel_kendaraans_url
    assert_selector "h1", text: "Tabel kendaraans"
  end

  test "should create tabel kendaraan" do
    visit tabel_kendaraans_url
    click_on "New tabel kendaraan"

    fill_in "Id kendaraan", with: @tabel_kendaraan.ID_Kendaraan
    fill_in "No kndrn", with: @tabel_kendaraan.No_Kndrn
    fill_in "No lambung", with: @tabel_kendaraan.No_Lambung
    fill_in "Warna", with: @tabel_kendaraan.Warna
    click_on "Create Tabel kendaraan"

    assert_text "Tabel kendaraan was successfully created"
    click_on "Back"
  end

  test "should update Tabel kendaraan" do
    visit tabel_kendaraan_url(@tabel_kendaraan)
    click_on "Edit this tabel kendaraan", match: :first

    fill_in "Id kendaraan", with: @tabel_kendaraan.ID_Kendaraan
    fill_in "No kndrn", with: @tabel_kendaraan.No_Kndrn
    fill_in "No lambung", with: @tabel_kendaraan.No_Lambung
    fill_in "Warna", with: @tabel_kendaraan.Warna
    click_on "Update Tabel kendaraan"

    assert_text "Tabel kendaraan was successfully updated"
    click_on "Back"
  end

  test "should destroy Tabel kendaraan" do
    visit tabel_kendaraan_url(@tabel_kendaraan)
    click_on "Destroy this tabel kendaraan", match: :first

    assert_text "Tabel kendaraan was successfully destroyed"
  end
end
