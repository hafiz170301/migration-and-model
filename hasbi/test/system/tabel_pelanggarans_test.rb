require "application_system_test_case"

class TabelPelanggaransTest < ApplicationSystemTestCase
  setup do
    @tabel_pelanggaran = tabel_pelanggarans(:one)
  end

  test "visiting the index" do
    visit tabel_pelanggarans_url
    assert_selector "h1", text: "Tabel pelanggarans"
  end

  test "should create tabel pelanggaran" do
    visit tabel_pelanggarans_url
    click_on "New tabel pelanggaran"

    fill_in "Id nota", with: @tabel_pelanggaran.ID_Nota
    fill_in "Lokasi", with: @tabel_pelanggaran.Lokasi
    fill_in "No nota", with: @tabel_pelanggaran.No_Nota
    fill_in "Pelanggaran", with: @tabel_pelanggaran.Pelanggaran
    fill_in "Tgl pelanggaran", with: @tabel_pelanggaran.Tgl_Pelanggaran
    fill_in "Tindakan", with: @tabel_pelanggaran.Tindakan
    click_on "Create Tabel pelanggaran"

    assert_text "Tabel pelanggaran was successfully created"
    click_on "Back"
  end

  test "should update Tabel pelanggaran" do
    visit tabel_pelanggaran_url(@tabel_pelanggaran)
    click_on "Edit this tabel pelanggaran", match: :first

    fill_in "Id nota", with: @tabel_pelanggaran.ID_Nota
    fill_in "Lokasi", with: @tabel_pelanggaran.Lokasi
    fill_in "No nota", with: @tabel_pelanggaran.No_Nota
    fill_in "Pelanggaran", with: @tabel_pelanggaran.Pelanggaran
    fill_in "Tgl pelanggaran", with: @tabel_pelanggaran.Tgl_Pelanggaran
    fill_in "Tindakan", with: @tabel_pelanggaran.Tindakan
    click_on "Update Tabel pelanggaran"

    assert_text "Tabel pelanggaran was successfully updated"
    click_on "Back"
  end

  test "should destroy Tabel pelanggaran" do
    visit tabel_pelanggaran_url(@tabel_pelanggaran)
    click_on "Destroy this tabel pelanggaran", match: :first

    assert_text "Tabel pelanggaran was successfully destroyed"
  end
end
