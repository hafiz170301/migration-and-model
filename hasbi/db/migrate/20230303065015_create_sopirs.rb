class CreateSopirs < ActiveRecord::Migration[7.0]
  def change
    create_table :sopirs do |t|
      t.string :sim
      t.string :nama_sopir
      t.string :alamat
      t.string :kota
      t.string :kd_pos
      t.integer :jk
      t.string :tb
      t.string :bb

      t.timestamps
    end
  end
end
