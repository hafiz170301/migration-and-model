class CreateSupervisers < ActiveRecord::Migration[7.0]
  def change
    create_table :supervisers do |t|
      t.string :area_spv

      t.timestamps
    end
  end
end
