class AddReference < ActiveRecord::Migration[7.0]
  def change
    add_reference :kendaraans, :superviser, index: true
    add_reference :pelanggarans, :sopir, index:true
    add_reference :pelanggarans, :kendaraan, index:true
  end
end
