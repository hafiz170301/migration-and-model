class CreatePelanggarans < ActiveRecord::Migration[7.0]
  def change
    create_table :pelanggarans do |t|
      t.string :nmr_nota
      t.string :tgl_pelanggaran
      t.string :lokasi
      t.string :cttn_pelanggaran
      t.string :tindakan

      t.timestamps
    end
  end
end
