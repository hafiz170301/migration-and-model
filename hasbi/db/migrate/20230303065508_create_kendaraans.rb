class CreateKendaraans < ActiveRecord::Migration[7.0]
  def change
    create_table :kendaraans do |t|
      t.string :nmr_kendaraan
      t.string :warna
      t.string :tipe
      t.string :tahun
      t.string :nmr_lambung

      t.timestamps
    end
  end
end
