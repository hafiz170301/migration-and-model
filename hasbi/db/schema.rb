# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_03_03_070407) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "kendaraans", force: :cascade do |t|
    t.string "nmr_kendaraan"
    t.string "warna"
    t.string "tipe"
    t.string "tahun"
    t.string "nmr_lambung"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "superviser_id"
    t.index ["superviser_id"], name: "index_kendaraans_on_superviser_id"
  end

  create_table "pelanggarans", force: :cascade do |t|
    t.string "nmr_nota"
    t.string "tgl_pelanggaran"
    t.string "lokasi"
    t.string "cttn_pelanggaran"
    t.string "tindakan"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "sopir_id"
    t.bigint "kendaraan_id"
    t.index ["kendaraan_id"], name: "index_pelanggarans_on_kendaraan_id"
    t.index ["sopir_id"], name: "index_pelanggarans_on_sopir_id"
  end

  create_table "sopirs", force: :cascade do |t|
    t.string "sim"
    t.string "nama_sopir"
    t.string "alamat"
    t.string "kota"
    t.string "kd_pos"
    t.integer "jk"
    t.string "tb"
    t.string "bb"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "supervisers", force: :cascade do |t|
    t.string "area_spv"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
