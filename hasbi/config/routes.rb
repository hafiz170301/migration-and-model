Rails.application.routes.draw do
  resources :tabel_pelanggarans
  resources :tabel_area_spvs
  resources :tabel_kendaraans
  resources :tabel_sopirs
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
